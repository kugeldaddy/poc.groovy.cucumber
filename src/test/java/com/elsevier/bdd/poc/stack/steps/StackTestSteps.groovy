package com.elsevier.bdd.poc.stack.steps

import cucumber.api.PendingException;

import static org.hamcrest.CoreMatchers.*;
import org.hamcrest.core.IsNull;
import org.junit.Test;

import com.elsevier.bdd.poc.stack.Stack

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

Stack stack;
def poppedElement;

Given(~/^I have a new stack$/) {
	->
	// Write code here that turns the phrase above into concrete actions
	stack = new Stack()
}

Then(~/^the stack should be empty$/) {
	->
	assertThat (stack, is(not(nullValue())))
	assertThat(stack.size(), is(equalTo(0)))
}

When(~/^I add "([^"]*)"$/) { String element ->
	assertThat (stack, is(not(nullValue())))

	stack.push(element);
}

Then(~/^the stack should contain (\d+) elements$/) { int nrOfElements ->
	assertThat (stack, is(not(nullValue())))
	assertThat(stack.size(), is(equalTo(nrOfElements)))
}

Then(~/^the last element should be "([^"]*)"$/) { String element ->
	assertThat (stack, is(not(nullValue())))
	assertThat(stack.peek(), is(equalTo(element)))
}

When(~/^I remove the last element$/) {
	->
	assertThat (stack, is(not(nullValue())))
	poppedElement = stack.pop();
}

Then(~/^the removed element should be "([^"]*)"$/) { String element ->
	assertThat (stack, is(not(nullValue())))
	assertThat(poppedElement, is(equalTo(element)))
}


