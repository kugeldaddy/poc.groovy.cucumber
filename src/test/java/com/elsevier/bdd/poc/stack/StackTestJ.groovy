package com.elsevier.bdd.poc.stack

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.Test

class StackTestJ {
	static String JONATHAN = "Jonathan";
	static String UWE = "Uwe";

	@Test
	public void newStackIsEmpty() {
		def stack = new Stack();

		assertThat(stack.size(), equalTo(0));
	}

	@Test
	public void correctSizeAfterPush() {
		def stack = new Stack();

		assertThat(stack.size(), equalTo(0));
		stack.push(JONATHAN);
		assertThat(stack.size(), equalTo(1));
		assertThat(stack.peek(), equalTo(JONATHAN));
		stack.push(UWE);
		assertThat(stack.size(), equalTo(2));
		assertThat(stack.peek(), equalTo(UWE));
	}

	@Test
	public void emptyStackAfterRemovingAllElements() {
		def stack = new Stack();

		assertThat(stack.size(), equalTo(0));
		stack.push(JONATHAN);
		assertThat(stack.size(), equalTo(1));
		assertThat(stack.peek(), equalTo(JONATHAN));

		stack.push(UWE);
		assertThat(stack.size(), equalTo(2));
		assertThat(stack.peek(), equalTo(UWE));
		assertThat(stack.pop(), equalTo(UWE));
		assertThat(stack.size(), equalTo(1));
		assertThat(stack.pop(), equalTo(JONATHAN));
		assertThat(stack.size(), equalTo(0));
	}
}
