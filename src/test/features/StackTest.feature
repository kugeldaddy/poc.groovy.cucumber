@Smoke
Feature: Stack test
  Evaluate the functionality of the Implemted Stack.
  - A new stack must be empty

  Scenario: New stack
    Given I have a new stack
    Then the stack should be empty

  Scenario: a stack with "n" elements
    Given I have a new stack
    When I add "Uwe"
    And I add "Alwine"
    Then the stack should contain 2 elements
    And the last element should be "Alwine"
    When I remove the last element
    Then the removed element should be "Alwine"
    And the stack should contain 1 elements
    And the last element should be "Uwe"
    When I remove the last element
    Then the removed element should be "Uwe"
    And the stack should be empty
