package com.elsevier.bdd.poc.stack

class Stack {
	def elements = [];

	def isEmpty() {
		elements.isEmpty();
	}

	def peek() {
		isEmpty() ? null : elements.get(elements.size() - 1);
	}

	def push(element) {
		elements.add(element);
	}

	def pop() {
		isEmpty() ? null : elements.remove(elements.size() - 1);
	}

	def size() {
		elements.size();
	}

	static void main(args) {
		def element
		def	stack = new Stack()

		println "Created a stack with size -> " + stack.size()

		println  "Pushing -> " + (element = "Andree")
		stack.push(element)

		println "Peeking on stack with size -> " + stack.size() + " -> " + stack.peek()

		println  "Pushing -> " + (element = "Uwe")
		stack.push(element)

		println "Peeking on stack with size -> " + stack.size() + " -> " + stack.peek()

		println "Popping stack -> " + stack.pop() + " new size is -> " + stack.size()
		println  "Popping stack -> " + stack.pop() + " new size is -> " + stack.size()
	}
}
